.. _linux:

*******************
Linux Distributions
*******************

.. toctree::
   :maxdepth: 2
   :hidden:
   :name: toc-linux

   ubuntu
   debian
   redhat
   suse